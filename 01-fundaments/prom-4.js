function SumLent(number){
    let promise = new Promise(function (resolve, reject){

        setTimeout(function (){
            resolve(number + 1);
        }, 800)

    });
    return promise;
}

function  SumRun(number){
    let promise = new Promise(function (resolve, reject){

        setTimeout(function (){
            resolve(number + 1);
        }, 400)

    });
    return promise;
}

Promise.race([SumLent(5), SumRun(10)])
    .then(resp => console.log(resp))
