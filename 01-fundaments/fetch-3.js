let users = {
    name: 'David valenzuela',
    age: 32
}

fetch('https://reqres.in/api/users',
    {method: 'POST',
        body: JSON.stringify(users),
        headers:{
        'Content-Type' : 'application/json'
        }
    })
    .then(resp => resp.json())
    .then(console.log)
    .catch(error => console.log(error))
