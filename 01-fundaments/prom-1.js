function Sum(number, callback){

    if(number >= 7){
        callback('Número muy alto');
        return;
    }

    setTimeout(()=>{
        //return number + 1;
        callback(number + 1)
    }, 800)

}

Sum(5, function (error, newValue){
    if(error){
        console.log(error);
        return;
    }
    Sum(newValue, function (error, newValue1){
        if(error){
            console.log(error);
            return;
        }
        Sum(newValue1, function (error, newValue2){
            if(error){
                console.log(error);
                return;
            }
                console.log(newValue2)
        })
    })
})

