let imagen = document.querySelector('img');

fetch('superman.webp')
    .then(resp => resp.blob())
    .then( img => {
        let imgPATH =  URL.createObjectURL(img);
        console.log(imgPATH)
        imagen.src =  imgPATH;
    })
