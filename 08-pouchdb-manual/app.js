

// Taller PouchDB

// 1- Create database
// Name:  messages
let db = new PouchDB('messages')


// Object save in data base
let messages = {
    _id: new Date().toISOString(),
    user: 'spiderman',
    messages: 'My aunt, I make one very rich pancakes.',
    async: false
};


// 2- Insert database

/*db.put(messages)
    .then(()=>console.log('Register Insert...'))
    .catch(()=>console.log('Error in register...'));*/

// 3- Read data offline
db.allDocs({include_docs: true, descending: false}, (error, doc) => {
    if(error) return;
    console.log(doc.rows);

})



// 4- Change value Async of all objects
// in DB TRUE

/*
db.allDocs({include_docs: true, descending: false}, (error, doc) => {
    if(error) return;
    doc.rows.forEach(row => {
        let doc = row.doc;
        doc.async = true;
        db.put(doc)
    });
})
*/




// 5- Erase or delete registers, point to point, valuations
// Which this Async
// Should be all update comments
// The input Async

db.allDocs({include_docs: true}, (error, doc)=>{
    if(error) return;
    doc.forEach(row =>{
        let doc = row.doc;
        if(doc.async){
            db.remove(doc);
        }
    })

})




