importScripts('js/sw-util.js');

const STATIC_CACHE = 'static-v1';
const DYNAMIC_CACHE = 'dynamic-v1';
const INMUTABLE_CACHE = 'inmutable-v1';

const APP_SHELL = [
    '/',
    'style/base.css',
    'js/base.js',
    'js/app.js',
    'style/bg.png'
];

const APP_SHELL_INMUTABLE = [
    'https://cdn.jsdelivr.net/npm/pouchdb@7.2.1/dist/pouchdb.min.js'
];

self.addEventListener('install', (e) => {

    const cacheStatic = caches.open(STATIC_CACHE)
        .then(cache => cache.addAll(APP_SHELL));

    const cacheInmutable = caches.open(INMUTABLE_CACHE)
        .then(cache => cache.addAll(APP_SHELL_INMUTABLE));


    e.waitUntil(Promise.all([cacheStatic, cacheInmutable]));
});


self.addEventListener('activate', (e) => {
    const resp = caches.keys().then(keys => {
        keys.forEach(key => {
            if (key !== STATIC_CACHE && key.includes('static')) {
                return caches.delete(key);
            }
            if (key !== DYNAMIC_CACHE && key.includes('dynamic')) {
                return caches.delete(key);
            }
        });
    });
    e.waitUntil(resp);
});

self.addEventListener('fetch', (e) => {

    if (!(e.request.url.indexOf('http') === 0)) return;

    const resp = caches.match(e.request).then(cache => {
        if (cache) {
            return cache;
        } else {
            return fetch(e.request).then(newResp => {
                return updateCacheDynamic(DYNAMIC_CACHE, e.request, newResp);
            });
        }
    });

    e.respondWith(resp);


});
