const CACHE_STACTIC_NAME = 'cache-static-v1';
const CACHE_DYNAMIC_NAME = 'cache-dynamic-v1';
const CACHE_INMUTABLE_NAME = 'cache-inmutable-v1';

const NUMBER_ITEMS_LIMIT = 50;

// Function clean caches
function cleanCaches(cacheName, numberItems) {
    caches.open(cacheName).then(cache => {
        cache.keys()
            .then(keys => {
                if(keys.length > numberItems){
                    cache.delete(keys[0])
                        .then(cleanCaches(cacheName, numberItems));
                }
            });
    });

}

self.addEventListener('install', e => {
    let cachePromise = caches.open(CACHE_STACTIC_NAME)
        .then(cache => {
            return cache.addAll([
                '/',
                '/index.html',
                '/css/style.css',
                '/img/main.jpg',
                '/js/app.js',
                '/img/no-img.jpg'
            ]);
        });


    let cacheInmutable = caches.open(CACHE_INMUTABLE_NAME)
        .then(cache => cache.add('https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css'));

    e.waitUntil(Promise.all([cachePromise, cacheInmutable]));

});

self.addEventListener('fetch', e => {

    // List cache strategy

    // 1- Cache Only
    // e.respondWith(caches.match(e.request));

    // 2- Cache with Network
   /* const resp = caches.match(e.request)
        .then(resp => {
            if (resp) return resp;
            // Not found File exist
            console.log('Not found File...');


            return fetch(e.request).then(newResp => {
                caches.open(CACHE_DYNAMIC_NAME).then(cache => {
                    cache.put(e.request, newResp);
                    cleanCaches(CACHE_DYNAMIC_NAME, NUMBER_ITEMS_LIMIT);
                });
                return newResp.clone();
            });
        });

    e.respondWith(resp);*/
    // 3- Network first Cache fallback
   /* const resp = fetch(e.request).then(resp => {
        if(!resp) return caches.match(e.request);
        caches.open(CACHE_DYNAMIC_NAME)
            .then((cache) => {
                cache.put(e.request, resp).then(() => {
                    cleanCaches(CACHE_DYNAMIC_NAME, NUMBER_ITEMS_LIMIT);
                });
            });
        return resp.clone();
    }).catch(() => {
        return caches.match(e.request);
    });


    e.respondWith(resp);*/

    // 4- Cache with network update

  /*  if(e.request.url.includes('bootstrap')){
        return e.respondWith(caches.match(e.request));
    }

    const resp = caches.open(CACHE_STACTIC_NAME).then(cache => {
        fetch(e.request)
            .then(newRes => cache.put(e.request, newRes));

        return cache.match(e.request);
    });


    e.respondWith(resp);*/

    // 5 - Cache & Network Race

    const resp = new Promise((resolve, reject )=> {
        let rejects = false;

        const failOne = () => {
            if(rejects) {
                if(/\.(png|jpg)$/i.test(e.request.url)){
                    resolve(caches.match('/img/no-img.jpg'));
                }else{
                    reject('Not found resp');
                }
            } else {
                rejects = true;
            }
        };

        fetch(e.request).then(resp => {
            (resp.ok) ? resolve(resp) : failOne();
        }).catch(failOne);

        caches.match(e.request).then( res => {
            res ? resolve(res) : failOne();
        }).catch(failOne);

    });

        e.respondWith(resp);

});
