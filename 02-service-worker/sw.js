self.addEventListener('fetch', event => {

    event.respondWith(
        fetch(event.request)
            .then(resp => (resp.ok)? resp : fetch('img/main.jpg'))

    );

})
