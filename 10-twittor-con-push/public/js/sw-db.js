// Util save pouch DB
const db = new PouchDB('messages');


function saveMessage( message ) {

    message._id = new Date().toISOString();

    return db.put( message ).then( () => {

        self.registration.sync.register('new-post');

        const newResp = { ok: true, offline: true };

        return new Response( JSON.stringify(newResp) );

    });

}


// Post Message of Api
function postMessage() {

    const posts = [];

    return db.allDocs({ include_docs: true }).then( docs => {


        docs.rows.forEach( row => {

            const doc = row.doc;

            const fetchPom =  fetch('api', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify( doc )
                }).then( () => {

                    return db.remove( doc );

                });
            
            posts.push( fetchPom );


        }); // fin del foreach

        return Promise.all( posts );

    });





}

