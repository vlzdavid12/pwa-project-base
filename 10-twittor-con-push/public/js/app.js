var url = window.location.href;
var swLocation = '/twittor/sw.js';

var swReg;

if (navigator.serviceWorker) {


    if (url.includes('localhost')) {
        swLocation = '/sw.js';
    }

    window.addEventListener('load', function () {
        navigator.serviceWorker.register(swLocation).then(function (reg) {
            swReg = reg;
            swReg.pushManager.getSubscription().then(verifySuscription);
        });
    });
}


//  References of jQuery

var titulo = $('#titulo');
var nuevoBtn = $('#nuevo-btn');
var salirBtn = $('#salir-btn');
var cancelarBtn = $('#cancel-btn');
var postBtn = $('#post-btn');
var avatarSel = $('#seleccion');
var timeline = $('#timeline');

var modal = $('#modal');
var modalAvatar = $('#modal-avatar');
var avatarBtns = $('.seleccion-avatar');
var txtMensaje = $('#txtMensaje');

var btnActivadas = $('.btn-noti-activadas');
var btnDesactivadas = $('.btn-noti-desactivadas');

// The user key hero
var usuario;



function crearMensajeHTML(mensaje, personaje) {

    var content = `
    <li class="animated fadeIn fast">
        <div class="avatar">
            <img src="img/avatars/${personaje}.jpg">
        </div>
        <div class="bubble-container">
            <div class="bubble">
                <h3>@${personaje}</h3>
                <br/>
                ${mensaje}
            </div>
            
            <div class="arrow"></div>
        </div>
    </li>
    `;

    timeline.prepend(content);
    cancelarBtn.click();

}


// Globals
function logIn(ingreso) {

    if (ingreso) {
        nuevoBtn.removeClass('oculto');
        salirBtn.removeClass('oculto');
        timeline.removeClass('oculto');
        avatarSel.addClass('oculto');
        modalAvatar.attr('src', 'img/avatars/' + usuario + '.jpg');
    } else {
        nuevoBtn.addClass('oculto');
        salirBtn.addClass('oculto');
        timeline.addClass('oculto');
        avatarSel.removeClass('oculto');

        titulo.text('Seleccione Personaje');

    }

}


// Select Persons
avatarBtns.on('click', function () {

    usuario = $(this).data('user');

    titulo.text('@' + usuario);

    logIn(true);

});

// Btn EXIT
salirBtn.on('click', function () {

    logIn(false);

});

// Btn Message
nuevoBtn.on('click', function () {

    modal.removeClass('oculto');
    modal.animate({
        marginTop: '-=1000px',
        opacity: 1
    }, 200);

});


// Btn Cancel Message
cancelarBtn.on('click', function () {
    if (!modal.hasClass('oculto')) {
        modal.animate({
            marginTop: '+=1000px',
            opacity: 0
        }, 200, function () {
            modal.addClass('oculto');
            txtMensaje.val('');
        });
    }
});

// Btn send
postBtn.on('click', function () {

    var mensaje = txtMensaje.val();
    if (mensaje.length === 0) {
        cancelarBtn.click();
        return;
    }

    var data = {
        mensaje: mensaje,
        user: usuario
    };


    fetch('api', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then(res => res.json())
        .then(res => console.log('app.js', res))
        .catch(err => console.log('app.js error:', err));


    crearMensajeHTML(mensaje, usuario);

});


// Get Message Server
function getMensajes() {

    fetch('api')
        .then(res => res.json())
        .then(posts => {
            //console.log(posts);
            posts.forEach(post =>
                crearMensajeHTML(post.mensaje, post.user));
        });

}

getMensajes();


// Detect Change Connection
function isOnline() {

    if (navigator.onLine) {
        // get connection
        // console.log('online');
        $.mdtoast('Online', {
            interaction: true,
            interactionTimeout: 1000,
            actionText: 'OK!'
        });


    } else {
        // Not have connection
        $.mdtoast('Offline', {
            interaction: true,
            actionText: 'OK',
            type: 'warning'
        });
    }

}

window.addEventListener('online', isOnline);
window.addEventListener('offline', isOnline);

isOnline();

// Push Notify
function verifySuscription(actives) {
    if (actives) {
        btnActivadas.removeClass('oculto');
        btnDesactivadas.addClass('oculto');
    } else {
        btnDesactivadas.removeClass('oculto');
        btnActivadas.addClass('oculto');
    }
}

function cancelSuscription() {
    swReg.pushManager.getSubscription().then(subs => {
        subs.unsubscribe()
            .then(() => verifySuscription(false));
    });
}


function sendNotify() {
    const notificationOpts = {
        body: 'This body notification.',
        icon: '/img/icons/icon-72x72.png'
    };
    const n = new Notification('Title Notification', notificationOpts);

    n.onclick = () => {
        console.log('Click Notify!');
    };
}


function notifyAction() {
    if (!window.notifyAction) {
        console.log('This navigator no support');
        return;
    }


    if (Notification.permission === 'granted') {
        //new Notification('Hey Well Come, new notification');
        sendNotify();
    } else if (Notification.permission === 'denied' || Notification.permission === 'default') {
        Notification.requestPermission(function (permission) {
            console.log(permission);
            if (permission === 'granted') {
                //new Notification('Hello new question authorization.');
                sendNotify();
            }
        });
    }
}

//notifyAction();

//Get Key
function getPublicKey() {
    return fetch('api/key')
        .then(resp => resp.arrayBuffer())
        // Return array, for unique UnitBArray
        .then(key => new Uint8Array(key));
}

getPublicKey().then(()=>null);

//Action btn click activation
btnDesactivadas.on('click', function () {
    if (!swReg) return console.log('Not register of SW');

    getPublicKey().then(function (key) {
        swReg.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: key
        }).then(resp => resp.toJSON())
            .then(subscription => {
                console.log(subscription);
                fetch('api/subscribe', {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify(subscription)
                }).then(verifySuscription)
                    .catch(cancelSuscription);
            });

    });


});

btnActivadas.on('click', function (){
    cancelSuscription();
});


