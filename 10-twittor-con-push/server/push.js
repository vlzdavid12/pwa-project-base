const fs = require('fs');
const vapid = require('./vapi.json');
const urlSafeBase64 = require('urlsafe-base64');

const webpush = require('web-push');

webpush.setVapidDetails(
    'mailto:vlzdavid12@outlook.com',
    vapid.publicKey,
    vapid.privateKey
);


let subscriptions = require('./subs-db.json');

module.exports.getKey = () =>{
    return urlSafeBase64.decode(vapid.publicKey);
};


module.exports.addSubscription = (subscription) => {
    subscriptions.push(subscription);

    fs.writeFileSync(`${__dirname}/subs-db.json`, JSON.stringify(subscriptions));
};

module.exports.sendPush = (post) => {
    const notifySend =  []; 
    subscriptions.forEach((subscription, index)=>{
        const pushProm = webpush.sendNotification(subscription, JSON.stringify(post))
            .then(()=>console.log('Notify send'))
            .catch((error)=>{
                console.log('Notify error filed');
                if(error.statusCode === 410){
                    subscriptions[index].borrar = true;
                }
            });

        notifySend.push(pushProm);
    });
    
    Promise.all(notifySend).then(()=>{
        subscriptions =  subscriptions.filter(subs => !subs.borrar);
        fs.writeFileSync(`${__dirname}/subs-db.json`, JSON.stringify(subscriptions));
    });
};
