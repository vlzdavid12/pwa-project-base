// Routes.js - Módulo de rutas
const express = require('express');
const router = express.Router();
const push = require('./push');


const mgs = [

    {
        _id: 'XXX',
        user: 'spiderman',
        mensaje: 'Hola Mundo'
    }

];


// Get messages
router.get('/', function (req, res) {
    // res.json('Get Messages');
    res.json(mgs);
});


// Post message
router.post('/', function (req, res) {

    const message = {
        message: req.body.mensaje,
        user: req.body.user
    };

    message.push(message);

    console.log(message);

    res.json({
        ok: true,
        message
    });
});


// Save in subscribe
router.post('/subscribe', function (req, res) {
    const subscription = req.body;

    push.addSubscription(subscription);

    res.json('Working Subscribe!');
});

// Save in subscribe
router.get('/key', function (req, res) {
    const key = push.getKey();
    res.send(key);
});


// Send notify push in persons key unique
// Controller direct with server
router.post('/push', (req, res) => {
    const post = {
        title: req.body.title,
        body: req.body.message,
        user: req.body.user
    };

    push.sendPush(post);

    res.json({status: 'ok', msg: post});
});


module.exports = router;
