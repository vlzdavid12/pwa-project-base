// Ciclo de vida del SW

self.addEventListener('install', event => {

    const install = new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('SW: Service Connect!');
            self.skipWaiting();
            resolve();
        }, 1);
    });

    event.waitUntil(install);

    //Active service
    //self.skipWaiting();

});

self.addEventListener('activate', event => {
    console.log('SW: Active service!');
});



// FETCH: driver dependency for HTTP
self.addEventListener('fetch', event => {

   /* //Cache strategy cache
    console.log('SW:', event.request.url);

    if(event.request.url.includes('api/products/3')){
        let resp = new Response(`{ok: false, messsage: 'Hola!'}`);
        event.respondWith(resp);
    }*/

});

// ASYNC: Recovery Info Offline
self.addEventListener('sync', event => {

  /*  console.log('Ready Connect!');
    console.log(event);
    console.log(event.tag);*/

});

// Push: Run Push Notification PWA
self.addEventListener('push', event => {
    console.log('Notify push basic...');
})

