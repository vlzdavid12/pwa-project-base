

// Detectar si podemos usar Service Workers
if ( navigator.serviceWorker ) {
    navigator.serviceWorker.register('/sw.js')
        .then(req=>{
            setTimeout(()=>{
                req.sync.register('post-cat');
                console.log('Send photo cat server');
            }, 3000);

            Notification.requestPermission().then(result => {
                console.log(result);
                req.showNotification('Hello Notify!')

            });
        });

}

/*if(window.SyncManager){

}*/

/*fetch('https://reqres.in/api/products/3')
    .then(resp => resp.text())
    .then(console.log);*/
