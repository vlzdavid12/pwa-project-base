const express = require('express');

const path = require('path');

const app = express();

const bodyParser = require('body-parser');

const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;


// Directory public
app.use(express.static(publicPath));

// Body Param Json
app.use(bodyParser.json());

// Support Encoded Body
app.use(bodyParser.urlencoded({extended: true}));

// Roots
const routes = require('./routes');
app.use('/api', routes );



app.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Service run, for ${ port }`);

});
