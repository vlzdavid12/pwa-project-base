// Routes.js - module Routs
let express = require('express');
let router = express.Router();


const messages =  [
  {
    _id: 'XXX',
    user: 'Spiderman',
    message: 'Hello new message of SuperHero in Spider Man!'
  },
  {
    _id: 'XXX',
    user: 'Ironman',
    message: 'Hello new message of SuperHero in Iron Man!'
  },
  {
    _id: 'XXX',
    user: 'Hulk',
    message: 'Hello new message of SuperHero in Hulk!'
  },
  {
    _id: 'XXX',
    user: 'Wolverine',
    message: 'Hello new message of SuperHero in Wolverine!'
  }
];




// Get Messages
router.get('/', function (req, res) {
  res.json(messages);
});

// Post Message
router.post('/', function(req, res){
  const msg =  {
    message: req.body.message,
    user: req.body.user
  };

  messages.push(msg);

  res.json({
    ok: true,
    messages
  });

});




module.exports = router;
