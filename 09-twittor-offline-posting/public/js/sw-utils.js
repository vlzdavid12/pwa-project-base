// Save cache Dynamic
function updateCacheDynamic(dynamicCache, req, res) {


    if (res.ok) {

        return caches.open(dynamicCache).then(cache => {

            cache.put(req, res.clone());

            return res.clone();

        });

    } else {
        return res;
    }

}

// Cache with network update
function updateCacheStatic(staticCache, req, APP_SHELL_INMUTABLE) {


    if (APP_SHELL_INMUTABLE.includes(req.url)) {
        // Not update of inmutable
        // console.log('Exist Inmutable', req.url );

    } else {
        // console.log('Update', req.url );
        return fetch(req)
            .then(res => {
                updateCacheDynamic(staticCache, req, res);
                return res;
            });
    }

}


// Update message in the API
function updateApiMessage(dynamicCache, req) {

    if (req.clone().method === 'POST') {
        //Post New message
        if (self.registration.sync) {
            return req.clone().text().then(body => {
                //console.log(body)
                const bodyObj = JSON.parse(body);
                return saveMessageDB(bodyObj);

            });
        } else {
            return fetch(req);
        }

    } else {
        return fetch(req).then(res => {
            if (res.ok) {
                updateCacheDynamic(dynamicCache, req, res.clone());
                return res;
            } else {
                return caches.match(req);
            }
        }).catch(() => {
            return caches.match(req);
        });
    }


}

