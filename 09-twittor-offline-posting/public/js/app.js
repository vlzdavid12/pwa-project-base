var url = window.location.href;
var swLocation = '/twittor/sw.js';


if (navigator.serviceWorker) {


    if (url.includes('localhost')) {
        swLocation = '/sw.js';
    }


    navigator.serviceWorker.register(swLocation);
}


// Reference Jquery

var $ = jQuery;

var titulo = $('#titulo');
var nuevoBtn = $('#nuevo-btn');
var salirBtn = $('#salir-btn');
var cancelarBtn = $('#cancel-btn');
var postBtn = $('#post-btn');
var avatarSel = $('#seleccion');
var timeline = $('#timeline');

var modal = $('#modal');
var modalAvatar = $('#modal-avatar');
var avatarBtns = $('.seleccion-avatar');
var txtMensaje = $('#txtMensaje');

// The user select Hero
var user;


function crearMensajeHTML(message, person) {
    var content = `
    <li class="animated fadeIn fast">
        <div class="avatar">
            <img src="img/avatars/${person}.jpg">
        </div>
        <div class="bubble-container">
            <div class="bubble">
                <h3>@${person}</h3>
                <br/>
                ${message}
            </div>
            
            <div class="arrow"></div>
        </div>
    </li>
    `;

    timeline.prepend(content);
    cancelarBtn.click();

}


// Globals
function logIn(ingreso) {

    if (ingreso) {
        nuevoBtn.removeClass('oculto');
        salirBtn.removeClass('oculto');
        timeline.removeClass('oculto');
        avatarSel.addClass('oculto');
        modalAvatar.attr('src', 'img/avatars/' + user + '.jpg');
    } else {
        nuevoBtn.addClass('oculto');
        salirBtn.addClass('oculto');
        timeline.addClass('oculto');
        avatarSel.removeClass('oculto');

        titulo.text('Seleccione Personaje');

    }

}


// Select Person
avatarBtns.on('click', function () {

    user = $(this).data('user');

    titulo.text('@' + user);

    logIn(true);

});

// Button exit
salirBtn.on('click', function () {

    logIn(false);

});

// Button new message
nuevoBtn.on('click', function () {

    modal.removeClass('oculto');
    modal.animate({
        marginTop: '-=1000px',
        opacity: 1
    }, 200);

});

// Button cancel message
cancelarBtn.on('click', function () {
    if (!modal.hasClass('oculto')) {
        modal.animate({
            marginTop: '+=1000px',
            opacity: 0
        }, 200, function () {
            modal.addClass('oculto');
            txtMensaje.val('');
        });
    }
});


// Button send message
postBtn.on('click', function () {

    var message = txtMensaje.val();
    if (message.length === 0) {
        cancelarBtn.click();
        return;
    }


    let data = {
        message,
        user,
    };

    fetch('api', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }).then(resp => resp.json())
        .then(resp => console.log('success APP.JS: ', resp))
        .catch(error => console.log('error APP.JS: ', error));


    crearMensajeHTML(message, user);

});


//Get messages of server
function getMessagesServer() {
    fetch('api')
        .then((resp) => resp.json())
        .then((posts) => {
            posts.forEach(post => crearMensajeHTML(post.message, post.user));
        });
}

getMessagesServer();

// Detect change connection

function isOnline(){
    if(navigator.onLine){
        console.log('This online');

        $.mdtoast('Online', {
            interaction: true,
            interactionTimeout: 1000,
            actionText: 'Ok!'
        });

    } else {
        console.log('This off online');
        $.mdtoast('Offline', {
            interaction: true,
            actionText: 'Ok!'
        });
    }
}
window.addEventListener('online', isOnline);
window.addEventListener('offline', isOnline);


