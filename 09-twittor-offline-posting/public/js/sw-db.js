const db = new PouchDB('message');

function saveMessageDB(message) {
    message._id = new Date().toISOString();
    return db.put(message)
        .then(() => {
            self.registration.sync.register('new_post');
            const newResp = {ok: true, offline: true};
            return new Response(JSON.stringify(newResp));
        });
}

function postMessage() {

    const posts = [];

    return db.allDocs({include_docs: true}).then(docs => {
        docs.rows.forEach(post => {
            const doc = post.doc;
            const fetchPost = fetch('api', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(doc)
            }).then(() => {
                return db.remove(doc);
            });
            // Insert array posts
            posts.push(fetchPost);
        });

        return Promise.all(posts);

    });


}
