import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Countrys} from '../interface/country';
import {Observable, Subscriber} from 'rxjs';
import {promise} from 'protractor';


@Injectable({
  providedIn: 'root'
})
export class CountrysService {

  private countrys: Countrys[] = [];

  constructor(private http: HttpClient) {
  }


  public getCountrys(): Promise<Countrys[]> {
    if (this.countrys.length > 0) {
      return Promise.resolve(this.countrys);
    }
    return new Promise((resolve) => {
      this.http.get<Subscriber<Countrys[]>>('https://restcountries.com/v2/lang/es')
        .subscribe((country: any) => {
          this.countrys = country;
          resolve(country);
        });
    });
  }


  public getCityID(id: string | null): Promise<Countrys | undefined> {
    if (this.countrys.length > 0) {
      const country = this.countrys.find(p => p.alpha3Code === id);
      return Promise.resolve(country);
    }
    return this.getCountrys().then(() => {
      const resp = this.countrys.find(p => p.alpha3Code === id);
      return Promise.resolve(resp);
    });
  }


}
