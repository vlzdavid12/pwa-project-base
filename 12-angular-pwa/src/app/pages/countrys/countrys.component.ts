import { Component, OnInit } from '@angular/core';
import {CountrysService} from '../../services/countrys.service';
import {Countrys} from '../../interface/country';

@Component({
  selector: 'app-countrys',
  templateUrl: './countrys.component.html',
  styleUrls: ['./countrys.component.scss']
})
export class CountrysComponent implements OnInit {

  countrys: Countrys[] = [];

  constructor(public countrysService: CountrysService) {}

  ngOnInit(): void {
    this.countrysService.getCountrys()
      .then(country => this.countrys =  country);
  }

}
