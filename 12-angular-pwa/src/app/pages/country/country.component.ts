import {Component, OnInit} from '@angular/core';
import {CountrysService} from '../../services/countrys.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Countrys} from '../../interface/country';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {

  country!: Countrys | undefined;

  constructor(private countrysService: CountrysService,
              private activateRouter: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    const id = this.activateRouter.snapshot.paramMap.get('id');
    // @ts-ignore
    this.countrysService.getCityID(id).then((country: Countrys | undefined) => {
     if (!country) { return this.router.navigateByUrl('/'); }
     this.country = country;
    });
  }

}
