import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CountrysComponent} from './pages/countrys/countrys.component';
import {CountryComponent} from './pages/country/country.component';

const routes: Routes = [
  {path: '', component: CountrysComponent},
  {path: 'country/:id', component: CountryComponent},
  {path: '**',  redirectTo: ''},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
